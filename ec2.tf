# AWS instance creation
resource "aws_instance" "os1" {
  ami                    = "ami-008fe2fc65df48dac" # Ubuntu 20.04 LTS
  instance_type          = "t3.small"
  key_name               = "tf-test-key"
  vpc_security_group_ids = [aws_security_group.allow_tls.id]
  tags = {
    Name = "TerraformOS"
  }
}

# IP of AWS instance retrieved
output "op1" {
  value = aws_instance.os1.public_ip
}

# IP of AWS instance copied to a file ip.txt in local system
resource "local_file" "ip" {
  content  = aws_instance.os1.public_ip
  filename = "ip.txt"
}

# Connecting to the Ansible control node using SSH connection
resource "null_resource" "nullremote1" {
  depends_on = [aws_instance.os1]

  connection {
    type        = "ssh"
    user        = "ubuntu" # Change user to 'ubuntu' for Ubuntu instances
    private_key = file("~/terraform/tf-test-key.pem")
    host        = aws_instance.os1.public_ip
  }

  # Copying the ip.txt file to the Ansible control node from local system
  provisioner "file" {
    source      = "ip.txt"
    destination = "/tmp/ip.txt"
  }
}

# Connecting to the Linux OS  
resource "null_resource" "nullremote2" {
  depends_on = [aws_instance.os1]

  connection {
    type        = "ssh"
    user        = "ubuntu" # Change user to 'ubuntu' for Ubuntu instances
    private_key = file("~/terraform/tf-test-key.pem")
    host        = aws_instance.os1.public_ip
  }

  # Install the Ansible playbook
  provisioner "remote-exec" {
    inline = [
      "sudo apt update -y",
      "sudo apt install -y ansible"
    ]
  }
  # Copy Ansible playbook and install Ansible
  provisioner "file" {
    source      = "installdocker.yml"
    destination = "/tmp/installdocker.yml"
  }
  # Execute Ansible playbook to install Docker using the ubuntu user and the IP address from /tmp/ip.txt
  provisioner "remote-exec" {
    inline = [
      "ansible-playbook /tmp/installdocker.yml -i /tmp/ip.txt"
    ]
  }
}

# Running Ansible playbook on remote Linux OS
resource "null_resource" "nullremote3" {
  depends_on = [null_resource.nullremote2] # Depends on nullremote2

  connection {
    type        = "ssh"
    user        = "ubuntu" # Change user to 'ubuntu' for Ubuntu instances
    private_key = file("~/terraform/tf-test-key.pem")
    host        = aws_instance.os1.public_ip
  }
  provisioner "file" {
    source      = "instance.yml"
    destination = "/tmp/instance.yml"
  }
  provisioner "file" {
    source      = "vars.yml"
    destination = "/tmp/vars.yml"
  }
  provisioner "remote-exec" {
    inline = [
      "ansible-playbook /tmp/instance.yml -i /tmp/ip.txt"
    ]
  }
}
