terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.11.0"
    }
  }
}

provider "aws" {
  region  = "us-west-2"
   access_key = "enter your access key"
  secret_key = "enter your secret key"
}
